<?php
/**
 * @file
 * Admin file for conceptmap.
 */

/**
 * Form builder function for conceptmap settings.
 *
 * @param array $form
 *   Structured array containing the elements and properties of the form.
 * @param array $form_state
 *   An array which stores information about the form.
 */
function conceptmap_settings_form($form, &$form_state) {
  $form = array();
  // Don't load javascript unless libraries module is present.
  if (module_exists('libraries')) {
    // Load libraries.
    conceptmap_load_js();
  }

  // Basic Settings.
  $form['conceptmap_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t("General Settings"),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['conceptmap_settings']['conceptmap_show_description'] = array(
    '#type' => 'checkbox',
    '#title' => t('ToDo: Show concept map term descriptions.'),
    '#default_value' => variable_get('conceptmap_show_description', FALSE),
    '#description' => t('Term descriptions may be large and/or include pictures, therefore the Concept Map can take a long time to load if you include the full descriptions.'),
  );

  $form['conceptmap_settings']['conceptmap_drag_restict'] = array(
    '#type' => 'checkbox',
    '#title' => t('Restrict tag positioning to owner'),
    '#default_value' => variable_get('conceptmap_drag_restict', TRUE),
    '#description' => t('Terms in an Concept Map can only be moved/positioned by the entity owner, therefore the Concept Map is static for others. If enabled, Concept Map tag positions are only saved for the entity owner.'),
  );

  // Allow admin to select where to enable concept mapping.
  // conceptmap_entity_settings_form($form, $form_state);

  // Default settings for concept mapping fieldsets.
  $form['conceptmap_fieldset_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t("Fieldset Settings"),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['conceptmap_fieldset_settings']['conceptmap_default_collapsed'] = array(
    '#type' => 'checkbox',
    '#title' => t("Collapse conceptmap form by default"),
    '#description' => t("Checking this setting will collapse the conceptmap entry form by default"),
    '#default_value' => variable_get('conceptmap_default_collapsed', 1),
  );

  // Allow admin to set design colors, font and size.
  conceptmap_design_settings_form($form, $form_state);

  return system_settings_form($form);
}

/**
 * Form builder function for conceptmap design settings for users.
 *
 * @param array $form
 *   Structured array containing the elements and properties of the form.
 * @param array $form_state
 *   An array which stores information about the form.
 */
function conceptmap_design_user_settings_form(&$form, &$form_state) {
  if (module_exists('color')) {
    $form['conceptmap_design'] = array(
      '#type' => 'fieldset',
      '#title' => t('Design colors'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#group' => 'additional_settings',
      '#tree' => TRUE,
    );
    $form['conceptmap_design']['colors'] = array(
      '#type' => 'fieldset',
      '#title' => t('Design colors'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
      '#tree' => TRUE,
    );
    $form['conceptmap_design']['colors']['conceptmap_font_color'] = array(
      '#type' => 'textfield',
      '#size' => 60,
      '#maxlength' => 128,
      '#title' => t('Font color of taxonomy terms text:'),
      '#default_value' => $form_state['conceptmap_font_color'],
      '#required' => FALSE,
      '#element_validate' => array('conceptmap_hexcolor_element_validate'),
    );
    $form['conceptmap_design']['colors']['conceptmap_tag_background_color'] = array(
      '#type' => 'textfield',
      '#size' => 60,
      '#maxlength' => 128,
      '#title' => t('Taxonomy terms background and relation color:'),
      '#default_value' => $form_state['conceptmap_tag_background_color'],
      '#required' => FALSE,
      '#element_validate' => array('conceptmap_hexcolor_element_validate'),
    );
    $form['conceptmap_design']['colors']['conceptmap_arrow_color'] = array(
      '#type' => 'textfield',
      '#size' => 60,
      '#maxlength' => 128,
      '#title' => t('Arrow color:'),
      '#default_value' => $form_state['conceptmap_arrow_color'],
      '#required' => FALSE,
      '#element_validate' => array('conceptmap_hexcolor_element_validate'),
    );
  }
}

/**
 * Form builder function for conceptmap design settings.
 *
 * @param array $form
 *   Structured array containing the elements and properties of the form.
 * @param array $form_state
 *   An array which stores information about the form.
 */
function conceptmap_design_settings_form(&$form, &$form_state) {
  $form['conceptmap_design'] = array(
    '#type' => 'fieldset',
    '#title' => t('Design settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $options = array();
  $options = range(1, 10, 0.1);
  $options = array_combine($options, $options);

  $form['conceptmap_design']['conceptmap_canvas_size'] = array(
    '#type' => 'select',
    '#title' => t('Pick the canvas aspect ratio.'),
    '#default_value' => variable_get('conceptmap_canvas_size', 3),
    '#options' => $options,
    '#description' => t('Pick the canvas aspect ratio. The <em>height</em> will be calculated from canvas <em>width</em>. <em>height = width/ratio</em>. <strong>Note: may reset tag positions, if modified.</strong>'),
    '#multiple' => FALSE,
    '#required' => TRUE,
  );

  $options = array(
    ''           => t('none'),
    'sans-serif' => t('sans-serif'),
    'monospace'  => t('monospace'),
    'fantasy'    => t('fantasy'),
    'cursive'    => t('cursive'),
  );

  $form['conceptmap_design']['conceptmap_font'] = array(
    '#type' => 'select',
    '#title' => t('Select the font for displaying concept maps.'),
    '#default_value' => variable_get('conceptmap_font', 'sans-serif'),
    '#options' => $options,
    '#description' => t('Set a font for your concept maps.'),
    '#multiple' => FALSE,
    '#required' => TRUE,
  );

  $options = array();
  $options = range(1, 48, 1);
  $options = array_combine($options, $options);
  $form['conceptmap_design']['conceptmap_font_size'] = array(
    '#title' => t('Set a font size for terms and relations.'),
    '#description' => t('Select the font size for displaying terms and relations.'),
    '#type' => 'select',
    '#options' => $options,
    '#required' => TRUE,
    '#default_value' => variable_get('conceptmap_font_size', 18),
  );

  $options = range(0, 50, 1);
  $options = array_combine($options, $options);
  $form['conceptmap_design']['conceptmap_arrow_size'] = array(
    '#title' => t('Set the size of the triangle displaying relations.'),
    '#description' => t('Select the triangle size for displaying relations.'),
    '#type' => 'select',
    '#options' => $options,
    '#required' => TRUE,
    '#default_value' => variable_get('conceptmap_arrow_size', 5),
  );

  $options = range(0, 10, 0.1);
  $options = array_combine($options, $options);
  $form['conceptmap_design']['conceptmap_text_margin'] = array(
    '#title' => t('Set a margin around the text.'),
    '#description' => t('Select the size of margin around the text.'),
    '#type' => 'select',
    '#options' => $options,
    '#required' => TRUE,
    '#default_value' => variable_get('conceptmap_text_margin', 4),
  );

  if (module_exists('color')) {
    $form['conceptmap_design']['colors'] = array(
      '#type' => 'fieldset',
      '#title' => t('Design colors'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );
    $form['conceptmap_design']['colors']['conceptmap_background_color'] = array(
      '#type' => 'textfield',
      '#size' => 60,
      '#maxlength' => 128,
      '#title' => t('Background color of concept maps.'),
      '#default_value' => variable_get('conceptmap_background_color', '#f6f6f2'),
      '#required' => FALSE,
      '#element_validate' => array('conceptmap_hexcolor_element_validate'),
    );
    $form['conceptmap_design']['colors']['conceptmap_font_color'] = array(
      '#type' => 'textfield',
      '#size' => 60,
      '#maxlength' => 128,
      '#title' => t('Font color of taxonomy terms text:'),
      '#default_value' => variable_get('conceptmap_font_color', '#ffffff'),
      '#required' => FALSE,
      '#element_validate' => array('conceptmap_hexcolor_element_validate'),
    );
    $form['conceptmap_design']['colors']['conceptmap_tag_background_color'] = array(
      '#type' => 'textfield',
      '#size' => 60,
      '#maxlength' => 128,
      '#title' => t('Taxonomy terms background and relation color:'),
      '#default_value' => variable_get('conceptmap_tag_background_color', '#0074bd'),
      '#required' => FALSE,
      '#element_validate' => array('conceptmap_hexcolor_element_validate'),
    );
    $form['conceptmap_design']['colors']['conceptmap_arrow_color'] = array(
      '#type' => 'textfield',
      '#size' => 60,
      '#maxlength' => 128,
      '#title' => t('Arrow color:'),
      '#default_value' => variable_get('conceptmap_arrow_color', '#0074bd'),
      '#required' => FALSE,
      '#element_validate' => array('conceptmap_hexcolor_element_validate'),
    );
    $form['conceptmap_design']['colors']['alpha'] = array(
      '#type' => 'fieldset',
      '#title' => t('Color alpha'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );
    $form['conceptmap_design']['colors']['alpha']['conceptmap_color_alpha'] = array(
      '#type' => 'textfield',
      '#size' => 60,
      '#maxlength' => 128,
      '#title' => t('Alpha value for colors, relative to current color range.'),
      '#default_value' => variable_get('conceptmap_color_alpha', 180),
      '#required' => FALSE,
    );
  }
}

/**
 * Form builder function for entity settings.
 *
 * @param array $form
 *   Structured array containing the elements and properties of the form.
 * @param array $form_state
 *   An array which stores information about the form.
 */
function conceptmap_entity_settings_form(&$form, &$form_state) {
  $entity_types = entity_get_info();

  // Entity Settings.
  $form['conceptmap_entity_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t("Entity Settings"),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['conceptmap_entity_settings']['description'] = array(
    '#markup' => 'Select the entities to use for concept mapping:',
  );

  // List all entities with field_tags.
  $fields_info = field_info_field('field_tags');
  $entity_types = entity_get_info();
  foreach ($fields_info['bundles'] as $key => $type) {
    $type_infos = entity_get_info($key);
    // FIXME: there must be a better way to find bundle['label']!
    foreach ($type as $ekey => $etype) {
      foreach ($entity_types as $key => $type) {
        foreach ($type['bundles'] as $bkey => $bundle) {
          if (($bkey == $etype) && ($bkey != 'conceptmap')) {
            $form['conceptmap_entity_settings']['conceptmap-' . $key . '-' . $etype] = array(
              '#type' => 'checkbox',
              '#title' => t('Enable concept mapping on :bundle :entity.', array(':bundle' => $type_infos['label'], ':entity' => $bundle['label'])),
              '#default_value' => variable_get('conceptmap-' . $key . '-' . $etype),
            );
          }
          if (($bkey == $etype) && ($bkey == 'conceptmap')) {
            $form['conceptmap_entity_settings']['conceptmap-' . $key . '-' . $etype] = array(
              '#type' => 'checkbox',
              '#required' => TRUE,
              '#disabled' => TRUE,
              '#default_value' => 1,
              '#attributes' => array('checked' => 'checked'),
              '#title' => t('Default concept mapping on :bundle :entity.', array(':bundle' => $type_infos['label'], ':entity' => $bundle['label'])),
              '#default_value' => variable_get('conceptmap-' . $key . '-' . $etype),
            );
          }
        }
      }
    }
  }
  $form['conceptmap_entity_settings']['annotation'] = array(
    '#markup' => '<p>Available are entities with a taxonomy term reference field to tags <em>field_tags</em>.</p>',
  );
}

/**
 * Function to validate values from the settings form.
 *
 * @param array $form
 *   Structured array containing the elements and properties of the form.
 * @param array $form_state
 *   An array which stores information about the form.
 */
function conceptmap_settings_form_validate($form, &$form_state) {
}
