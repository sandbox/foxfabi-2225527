# Concept Map module for Drupal

## Content of this file
 - Introduction
 - Requirements
 - Installation
 - Configuration
 - How it works
 - Maintainers

## Introduction
Concept Map is a graphical tool to organize, structure and visualize
knowledge using taxonomy terms. Using the tags referenced by an
entity, allowed users can connect taxonomy terms naming the
relation, building a map of knowledge.

## Requirements
 - Entity API
 - Field (Core)
 - Taxonomy (field_tags) (Core)
 - Libraries API

#### Required components
To use Concept Map in Drupal, you will need to download processing.js.

## Installation
 - Enable required modules: Entity API, Libraries.
 - Visit http://processingjs.org/ and place processing.js to libraries folder.
 - Enable Concept Map module

## Configuration
 - Configure Concept Map settings via /admin/config/user-interface/conceptmap.
   - Set aspect ratio, font type and colors.
   - May allow unrestricted term positioning to users
 - Check permission to administer, add relations and view Concept Map content:
   - Administer Concept Map (administrator)
   - Create concept map relations (authenticated user)
   - View concept map relations (authenticated user)
 - May need to aggregate CSS/JS cache.
 
### How it works
 - Add a new Concept Map node. Set a title, add some tags.
   - After saving the node, a concept mapping fieldset will appear. Open it :)
   - The same functionality is provided by others enabled entities.
 - Add new relations to Concept Map by selecting the source and destination tag.
   - Describe the relation. After saving a fieldset appears with the Concept Map.
 - Organize and structure your tags.
   - Move yours tags in the canvas and place it everywhere.
   - Visualize your knowledge.

## Maintainers
Current maintainers:

 - Fabian Dennler (foxfabi) - https://www.drupal.org/user/240264/

This project has been sponsored by:

 - Fabian Dennler / @foxfabi / fabforge.ch
