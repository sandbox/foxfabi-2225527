<?php
/**
 * @file
 * Contains ConceptMap Controllers.
 */

/**
 * ConceptMapSettingControllerInterface definition.
 */
interface ConceptMapSettingControllerInterface
  extends DrupalEntityControllerInterface {

  /**
   * Create an entity.
   */
  public function create();
  /**
   * Save an entity.
   */
  public function save($entity);
  /**
   * Delete an entity.
   */
  public function delete($entity);

}

/**
 * ConceptMapSettingController extends DrupalDefaultEntityController.
 */
class ConceptMapSettingController
  extends DrupalDefaultEntityController
  implements ConceptMapSettingControllerInterface {

  /**
   * Create and return a new conceptmap_setting entity.
   */
  public function create() {
    $entity = new stdClass();
    $entity->type = 'conceptmap_setting';
    $entity->name = '';
    $entity->value = '';

    $entity->bundle_type = 'conceptmap_setting_entry';
    $entity->item_description = t('Concept Map setting');
    return $entity;
  }

  /**
   * Saves the custom fields using drupal_write_record().
   */
  public function save($entity) {
    // Invoke hook_entity_presave().
    module_invoke_all('entity_presave', $entity, 'conceptmap_setting');
    // The 'primary_keys' argument determines whether this will be an insert
    // or an update. So if the entity already has an ID, we'll specify
    // basic_id as the key.
    $primary_keys = isset($entity->etid) ? 'etid' : array();
    if (empty($primary_keys)) {
      $entity->created = REQUEST_TIME;
    }
    $entity->changed = REQUEST_TIME;
    // Write out the entity record.
    drupal_write_record('conceptmap_setting', $entity, $primary_keys);
    // We're going to invoke either hook_entity_update() or
    // hook_entity_insert(), depending on whether or not this is a
    // new entity. We'll just store the name of hook_entity_insert()
    // and change it if we need to.
    $invocation = 'entity_insert';
    // Now we need to either insert or update the fields which are
    // attached to this entity. We use the same primary_keys logic
    // to determine whether to update or insert, and which hook we
    // need to invoke.
    if (empty($primary_keys)) {
      field_attach_insert('conceptmap_setting', $entity);
    }
    else {
      field_attach_update('conceptmap_setting', $entity);
      $invocation = 'entity_update';
    }
    // Invoke either hook_entity_update() or hook_entity_insert().
    module_invoke_all($invocation, $entity, 'conceptmap_setting');
    return $entity;
  }

  /**
   * Delete a single entity.
   *
   * Really a convenience function for deleteMultiple().
   */
  public function delete($entity) {
    $this->deleteMultiple(array($entity));
  }

  /**
   * Delete one or more conceptmap_entry_position entities.
   *
   * Deletion is unfortunately not supported in the base
   * DrupalDefaultEntityController class.
   *
   * @param array $entities
   *   An array of entity IDs or a single numeric ID.
   */
  public function deleteMultiple($entities) {
    $teids = array();
    if (!empty($entities)) {
      $transaction = db_transaction();
      try {
        foreach ($entities as $entity) {
          module_invoke_all('conceptmap_setting_delete', $entity);
          // Invoke hook_entity_delete().
          module_invoke_all('entity_delete', $entity, 'conceptmap_setting');
          field_attach_delete('conceptmap_setting', $entity);
          $ids[] = $entity->etid;
        }
        db_delete('conceptmap_setting')
          ->condition('etid', $ids, 'IN')
          ->execute();

      }
      catch (Exception $e) {
        $transaction->rollback();
        watchdog_exception('conceptmap', $e);
        throw $e;
      }
    }
  }
}

/**
 * ConceptMapRelationControllerInterface definition.
 */
interface ConceptMapRelationControllerInterface
  extends DrupalEntityControllerInterface {

  /**
   * Create an entity.
   */
  public function create();
  /**
   * Save an entity.
   */
  public function save($entity);
  /**
   * Delete an entity.
   */
  public function delete($entity);

}

/**
 * ConceptMapRelationController extends DrupalDefaultEntityController.
 *
 * Our subclass of DrupalDefaultEntityController lets us add a few
 * important create, update, and delete methods.
 */
class ConceptMapRelationController
  extends DrupalDefaultEntityController
  implements ConceptMapRelationControllerInterface {

  /**
   * Create and return a new conceptmap relation entity.
   */
  public function create() {
    $entity = new stdClass();
    $entity->type = 'conceptmap_relation';
    $entity->bundle_type = 'conceptmap_relation';
    $entity->description = '';
    return $entity;
  }

  /**
   * Saves the custom fields using drupal_write_record().
   */
  public function save($entity) {
    // Invoke hook_entity_presave().
    module_invoke_all('entity_presave', $entity, 'conceptmap_relation');
    // The 'primary_keys' argument determines whether this will be an insert
    // or an update. So if the entity already has an ID, we'll specify
    // basic_id as the key.
    $primary_keys = isset($entity->relation_id) ? 'relation_id' : array();
    if (empty($primary_keys)) {
      $entity->created = REQUEST_TIME;
    }
    $entity->changed = REQUEST_TIME;
    // Write out the entity record.
    drupal_write_record('conceptmap_relation', $entity, $primary_keys);
    // We're going to invoke either hook_entity_update() or
    // hook_entity_insert(), depending on whether or not this is a
    // new entity. We'll just store the name of hook_entity_insert()
    // and change it if we need to.
    $invocation = 'entity_insert';
    // Now we need to either insert or update the fields which are
    // attached to this entity. We use the same primary_keys logic
    // to determine whether to update or insert, and which hook we
    // need to invoke.
    if (empty($primary_keys)) {
      field_attach_insert('conceptmap_relation', $entity);
    }
    else {
      field_attach_update('conceptmap_relation', $entity);
      $invocation = 'entity_update';
    }
    // Invoke either hook_entity_update() or hook_entity_insert().
    module_invoke_all($invocation, $entity, 'conceptmap_relation');
    return $entity;
  }

  /**
   * Add a new conceptmap entry position.
   */
  public function addPosition($uid, $tid, $entity_type, $entity_bundle, $entity_id) {
    $position = entity_get_controller('conceptmap_tag_position')->create();
    $position->uid = $uid;
    $position->entity_id = $entity_id;
    $position->entity_type = $entity_type;
    $position->entity_bundle = $entity_bundle;
    $position->tid = $tid;
    $position->x = 0;
    $position->y = 0;
    $position->z = 0;
    $position = conceptmap_tag_position_save($position);
  }

  /**
   * Check if the term already exists as conceptmap entry position.
   */
  public function getPosition($uid, $tid, $entity_type, $entity_bundle, $entity_id) {
    $entries = array();
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'conceptmap_tag_position');
    $query->propertyCondition('tid', $tid);
    $query->propertyCondition('entity_id', $entity_id);
    $query->propertyCondition('entity_type', $entity_type);
    $query->propertyCondition('entity_bundle', $entity_bundle);
    $query->propertyCondition('uid', $uid);
    $result = $query->execute();

    if (isset($result['conceptmap_tag_position'])) {
      $entry_ids = array_keys($result['conceptmap_tag_position']);
      $entries = entity_load('conceptmap_tag_position', $entry_ids);
    }
    return $entries;
  }

  /**
   * Prepare conceptmap entry position query for given entity.
   */
  private function defaultPositionsQuery($entity) {
    // Default query used later.
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'conceptmap_relation')
      ->propertyCondition('relation_id', $entity->relation_id, '!=')
      ->propertyCondition('entity_type', $entity->entity_type)
      ->propertyCondition('entity_bundle', $entity->entity_bundle)
      ->propertyCondition('entity_id', $entity->entity_id)
      ->propertyCondition('uid', $entity->uid);
    return $query;
  }

  /**
   * Delete conceptmap_entry_positions for unused terms relations.
   *
   * On deleting a relation, we check if the associated terms are used
   * in other relations, if not so, we can delete the position for the entity.
   */
  private function unusedTermPositions($entity) {
    $posentries = array();
    $delete['source'] = TRUE;
    $delete['destination'] = TRUE;

    // Check if other relations are using the same source term.
    $same_term = $this->defaultPositionsQuery($entity);
    $same_term->propertyCondition('source_tid', $entity->source_tid);
    $result = $same_term->execute();
    if (isset($result['conceptmap_relation'])) {
      $entry_ids = array_keys($result['conceptmap_relation']);
      $delete['source'] = FALSE;
    }

    // Check if other relations are using the same source term as destination.
    $same_term = $this->defaultPositionsQuery($entity);
    $same_term->propertyCondition('destination_tid', $entity->source_tid);
    $result = $same_term->execute();
    if (isset($result['conceptmap_relation'])) {
      $entry_ids = array_keys($result['conceptmap_relation']);
      $delete['source'] = FALSE;
    }

    // Check if other relations are using the destination term.
    $same_term = $this->defaultPositionsQuery($entity);
    $same_term->propertyCondition('destination_tid', $entity->destination_tid);
    $result = $same_term->execute();
    if (isset($result['conceptmap_relation'])) {
      $entry_ids = array_keys($result['conceptmap_relation']);
      $delete['destination'] = FALSE;
    }

    // Check if other relations are using the destination term as source.
    $same_term = $this->defaultPositionsQuery($entity);
    $same_term->propertyCondition('source_tid', $entity->destination_tid);
    $result = $same_term->execute();
    if (isset($result['conceptmap_relation'])) {
      $entry_ids = array_keys($result['conceptmap_relation']);
      $delete['destination'] = FALSE;
    }

    if ($delete['source']) {
      // No relations on entity is using this term, delete position.
      $query = new EntityFieldQuery();
      $query->entityCondition('entity_type', 'conceptmap_tag_position')
        ->propertyCondition('tid', $entity->source_tid)
        ->propertyCondition('entity_type', $entity->entity_type)
        ->propertyCondition('entity_bundle', $entity->entity_bundle)
        ->propertyCondition('entity_id', $entity->entity_id)
        ->propertyCondition('uid', $entity->uid);
      $result = $query->execute();
      if (isset($result['conceptmap_tag_position'])) {
        $entry_ids = array_keys($result['conceptmap_tag_position']);
        $posentries += entity_load('conceptmap_tag_position', $entry_ids);
      }
    }

    if ($delete['destination']) {
      // No relations on entity is using this term, delete position.
      $query = new EntityFieldQuery();
      $query->entityCondition('entity_type', 'conceptmap_tag_position')
        ->propertyCondition('tid', $entity->destination_tid)
        ->propertyCondition('entity_type', $entity->entity_type)
        ->propertyCondition('entity_bundle', $entity->entity_bundle)
        ->propertyCondition('entity_id', $entity->entity_id)
        ->propertyCondition('uid', $entity->uid);
      $result = $query->execute();
      if (isset($result['conceptmap_tag_position'])) {
        $entry_ids = array_keys($result['conceptmap_tag_position']);
        $posentries += entity_load('conceptmap_tag_position', $entry_ids);
      }
    }

    return $posentries;
  }

  /**
   * Delete a single entity.
   *
   * Really a convenience function for delete_multiple().
   */
  public function delete($entity) {
    $this->deleteMultiple(array($entity));
  }

  /**
   * Delete one or more conceptmap_relation entities.
   *
   * @param array $entities
   *   An array of entity IDs or a single numeric ID.
   */
  public function deleteMultiple($entities) {
    $cmapids = array();
    $cmposids = array();
    if (!empty($entities)) {
      $transaction = db_transaction();
      try {

        // Delete all given entities.
        foreach ($entities as $entity) {
          module_invoke_all('conceptmap_relation_delete', $entity);
          // Invoke hook_entity_delete().
          module_invoke_all('entity_delete', $entity, 'conceptmap_relation');
          field_attach_delete('conceptmap_relation', $entity);
          $cmposids = $this->unusedTermPositions($entity);
          $cmapids[] = $entity->relation_id;
        }
        // Delete all unused entry positions.
        if (count($cmposids) > 0) {
          entity_get_controller('conceptmap_tag_position')->deleteMultiple($cmposids);
        }

        db_delete('conceptmap_relation')
          ->condition('relation_id', $cmapids, 'IN')
          ->execute();

      }
      catch (Exception $e) {
        $transaction->rollback();
        watchdog_exception('conceptmap', $e);
        throw $e;
      }
    }
  }
}

/**
 * ConceptMapTagPositionControllerInterface definition.
 */
interface ConceptMapTagPositionControllerInterface
  extends DrupalEntityControllerInterface {
  /**
   * Create an entity.
   */
  public function create();
  /**
   * Save an entity.
   */
  public function save($entity);
  /**
   * Delete an entity.
   */
  public function delete($entity);
}

/**
 * ConceptMapEntryPositionController extends DrupalDefaultEntityController.
 *
 * Our subclass of DrupalDefaultEntityController lets us add a few
 * important create, update, and delete methods.
 */
class ConceptMapTagPositionController
  extends DrupalDefaultEntityController
  implements ConceptMapTagPositionControllerInterface {

  /**
   * Create and return a new conceptmap_entry_position entity.
   */
  public function create() {
    $entity = new stdClass();
    $entity->type = 'conceptmap_tag_position';
    $entity->x = 0;
    $entity->y = 0;
    $entity->z = 0;

    $entity->bundle_type = 'conceptmap_relation';
    $entity->item_description = t('Concept Map tag position');
    return $entity;
  }

  /**
   * Saves the custom fields using drupal_write_record().
   */
  public function save($entity) {

    // If our entity has no position, then we need to give it one.
    if (empty($entity->x)) {
      $entity->x = 0;
      $entity->y = 0;
      $entity->z = 0;
    }
    // Invoke hook_entity_presave().
    module_invoke_all('entity_presave', $entity, 'conceptmap_tag_position');
    // The 'primary_keys' argument determines whether this will be an insert
    // or an update. So if the entity already has an ID, we'll specify
    // basic_id as the key.
    $primary_keys = isset($entity->position_id) ? 'position_id' : array();
    if (empty($primary_keys)) {
      $entity->created = REQUEST_TIME;
    }
    $entity->changed = REQUEST_TIME;
    // Write out the entity record.
    drupal_write_record('conceptmap_tag_position', $entity, $primary_keys);
    // We're going to invoke either hook_entity_update() or
    // hook_entity_insert(), depending on whether or not this is a
    // new entity. We'll just store the name of hook_entity_insert()
    // and change it if we need to.
    $invocation = 'entity_insert';
    // Now we need to either insert or update the fields which are
    // attached to this entity. We use the same primary_keys logic
    // to determine whether to update or insert, and which hook we
    // need to invoke.
    if (empty($primary_keys)) {
      field_attach_insert('conceptmap_tag_position', $entity);
    }
    else {
      field_attach_update('conceptmap_tag_position', $entity);
      $invocation = 'entity_update';
    }
    // Invoke either hook_entity_update() or hook_entity_insert().
    module_invoke_all($invocation, $entity, 'conceptmap_tag_position');
    return $entity;
  }

  /**
   * Delete a single entity.
   *
   * Really a convenience function for deleteMultiple().
   */
  public function delete($entity) {
    $this->deleteMultiple(array($entity));
  }

  /**
   * Delete one or more conceptmap_entry_position entities.
   *
   * Deletion is unfortunately not supported in the base
   * DrupalDefaultEntityController class.
   *
   * @param array $entities
   *   An array of entity IDs or a single numeric ID.
   */
  public function deleteMultiple($entities) {
    $teids = array();
    if (!empty($entities)) {
      $transaction = db_transaction();
      try {
        foreach ($entities as $entity) {
          module_invoke_all('conceptmap_tag_position_delete', $entity);
          // Invoke hook_entity_delete().
          module_invoke_all('entity_delete', $entity, 'conceptmap_tag_position');
          field_attach_delete('conceptmap_tag_position', $entity);
          $cmposid[] = $entity->position_id;
        }
        db_delete('conceptmap_tag_position')
          ->condition('position_id', $cmposid, 'IN')
          ->execute();

      }
      catch (Exception $e) {
        $transaction->rollback();
        watchdog_exception('conceptmap', $e);
        throw $e;
      }
    }
  }
}
