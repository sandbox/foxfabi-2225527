<?php
/**
 * @file
 * Additional functions and queries for the Concept Map module.
 */

/**
 * Helper function to determine if a user has conceptmaps already.
 */
function _conceptmap_post_exists($account) {
  return (bool) db_select('node', 'n')
    ->fields('n', array('nid'))
    ->condition('type', 'conceptmap')
    ->condition('uid', $account->uid)
    ->condition('status', 1)
    ->range(0, 1)
    ->addTag('node_access')
    ->execute()
    ->fetchField();
}

/**
 * Fetchs all Taxonomy Term IDs from Entity Object. 
 *
 * All fields of field type "taxonomy_term_reference" will be included.
 *
 * @param string $entity_type
 *   Type of entity to get items.
 * @param Object $entity
 *   Entity to get items.
 * @param string $field
 *   Name of entity field to get items.
 *
 * @return array
 *   Array with tids of entity
 */
function _conceptmap_get_entity_terms($entity_type, $entity, $field) {
  global $user;
  $tids = array();
  $src_tags = field_get_items($entity_type, $entity, $field);
  if (is_array($src_tags)) {
    foreach ($src_tags as $key) {
      $term = taxonomy_term_load($key['tid']);
      if (isset($term->name)) {
        $name = _conceptmap_get_tag_name($key['tid']);
        $tids[$key['tid']] = $name;
      }
    }
  }
  return array_unique($tids);
}

/**
 * Query conceptmap entities for term.
 */
function _conceptmap_query_unique_entry($source_tid, $destination_tid, $entity_id) {
  global $user;
  $entries = NULL;
  // Fetch conceptmap entity entries for logged in user.
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'conceptmap_relation')
    ->propertyCondition('entity_id', $entity_id)
    ->propertyCondition('source_tid', $source_tid)
    ->propertyCondition('destination_tid', $destination_tid);
  $result = $query->execute();

  if (isset($result['conceptmap_relation'])) {
    $entry_ids = array_keys($result['conceptmap_relation']);
    $entries = entity_load('conceptmap_relation', $entry_ids);
  }

  return $entries;
}

/**
 * Main concept mapping form.
 *
 * Showed on taxonomy term view page.
 *  - Lists existing conceptmap entries
 *  - Add a form to add new entries
 */
function _conceptmap_relation_form(&$form, $entity_id, $type, $entity, $info) {
  $src_tags = _conceptmap_get_entity_terms($type, $entity, 'field_tags');
  if (!is_array($src_tags) || count($src_tags) <= 0) {
    return;
  }

  $form['adminform'] = array(
    '#type' => 'fieldset',
    '#title' => t('Concept Mapping'),
    '#collapsible' => TRUE,
    '#collapsed' => variable_get('conceptmap_default_collapsed', 0),
  );

  if (variable_get('conceptmap_default_collapsed', 0)) {
    $form['adminform']['#attributes'] = array('class' => array('collapsible collapsed'));
  }
  else {
    $form['adminform']['#attributes'] = array('class' => array('collapsible'));
  }

  $entries = _conceptmap_query_entries($entity_id, $info['entity_type'], $info['entity_bundle']);

  // Display the concept map table.
  if (isset($entries) && count($entries) > 0) {
    $form['adminform']['entries'] = array(
      '#markup' => theme('conceptmap_relation_table', $entries),
      '#weight' => 2,
    );
  }

  // Add entity form.
  $new_entity = entity_get_controller('conceptmap_relation')->create();
  if (is_array($src_tags) && count($src_tags) > 0) {
    $form['adminform']['entry_form'] = drupal_get_form('conceptmap_relation_form', $info, $new_entity);
    $form['adminform']['entry_form']['#weight'] = 5;
  }

  return array($src_tags, $entries);
}

/**
 * Concept mapping relation form.
 */
function _conceptmap_term_relation_form($info, $entity, $src_tags, $dst_tags, &$form = array()) {
  // Set up the entity.
  $form['entity_info'] = array(
    '#type' => 'value',
    '#value' => $info,
  );

  if ($info['task'] == 'edit') {
    $title = t('Edit Relation');
  }
  else {
    $title = t('Add Relation');
  }

  $form['new_relation'] = array(
    '#type' => 'fieldset',
    '#title' => $title,
    '#collapsible' => TRUE,
    '#weight' => 200,
    '#prefix' => '<div id="conceptmap-entry-form-wrapper">',
    '#suffix' => '</div>',
  );

  if ($info['task'] == 'add') {
    $form['new_relation']['#attached']['css'] = array(
      drupal_get_path('module', 'conceptmap') . '/css/conceptmap.css',
    );
    if (variable_get('conceptmap_default_collapsed', 0)) {
      $form['new_relation']['#attributes'] = array('class' => array('collapsible collapsed'));
    }
    else {
      $form['new_relation']['#attributes'] = array('class' => array('collapsible'));
    }
  }
  else {
    $form['uid'] = array(
      '#type' => 'value',
      '#value' => $entity->uid,
    );
  }

  $form['new_relation']['source_tid'] = array(
    '#type' => 'select',
    '#title' => t('Source'),
    '#options' => $src_tags,
    '#default_value' => isset($entity->source_tid) ? $entity->source_tid : NULL,
    '#element_validate' => array('conceptmap_term_validate'),
  );

  $form['new_relation']['description'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('Description'),
    '#size' => 25,
    '#default_value' => isset($entity->description) ? $entity->description : t('Describe the relation'),
  );

  $form['new_relation']['destination_tid'] = array(
    '#type' => 'select',
    '#title' => t('Destination'),
    '#options' => $dst_tags,
    '#default_value' => isset($entity->destination_tid) ? $entity->destination_tid : NULL,
    '#element_validate' => array('conceptmap_term_validate'),
  );
}


/**
 * Validate form submit handler.
 */
function conceptmap_term_validate() {
  // Should check if the taxonomy term id provided exists?
}

/**
 * Set or load default settings for concept maps.
 */
function _conceptmap_load_settings() {
  // Check for global settings.
  $conf['drag']['resticted'] = variable_get('conceptmap_drag_restict', TRUE);
  $conf['font']['size'] = variable_get('conceptmap_font_size', 12);
  $conf['font']['type'] = variable_get('conceptmap_font', 'sans-serif');
  if (empty($conf['font']['type'])) {
    drupal_set_message(t('Set a Concept Map <strong>font type</strong>.') . ' ' . l(t('Concept Map settings'), 'admin/config/user-interface/conceptmap'), 'error');
  }
  // Default background color.
  $conf['color']['background'] = variable_get('conceptmap_background_color', '#ffffff');
  // Default font color.
  $conf['color']['font'] = variable_get('conceptmap_font_color', '#ffffff');
  // Default tag color.
  $conf['color']['tag'] = variable_get('conceptmap_tag_background_color', '#0074bd');
  // Default arrow color.
  $conf['color']['arrow'] = variable_get('conceptmap_arrow_color', '#0074bd');
  // Default alpha value.
  $conf['color']['alpha'] = variable_get('conceptmap_color_alpha', 180);
  // Default aspect ratio divisor.
  $conf['canvas']['size'] = variable_get('conceptmap_canvas_size', 3);
  // Default arrow size.
  $conf['arrow']['size'] = variable_get('conceptmap_arrow_size', 5);
  // Default arrow size.
  $conf['text']['margin'] = variable_get('conceptmap_text_margin', 5);
  return $conf;
}

/**
 * Update concept map term position.
 */
function _conceptmap_set_tag_position($entity_type, $entity_bundle, $entity_id, $tid, $x, $y) {
  global $user;
  $conf = _conceptmap_load_settings();
  if ($conf['drag']['resticted']) {
    $positions = entity_get_controller('conceptmap_relation')->getPosition($user->uid, $tid, $entity_type, $entity_bundle, $entity_id, FALSE);
  }
  else {
    $entity_main = entity_load_single($entity_type, $entity_id);
    $positions = entity_get_controller('conceptmap_relation')->getPosition($entity_main->uid, $tid, $entity_type, $entity_bundle, $entity_id, FALSE);
  }
  foreach ($positions as $pos_key => $pos_entity) {
    $position = $pos_entity;
  }
  $position->x = $x;
  $position->y = $y;
  $position->z = 0;
  $position = conceptmap_tag_position_save($position);
  return $position;
}

/**
 * Loads the name of a taxonomy term.
 */
function _conceptmap_get_tag_name($tid) {
  global $user;
  $term = taxonomy_term_load($tid);
  $name = $term->name;
  return $name;
}
