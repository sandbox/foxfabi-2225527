<?php
/**
 * @file
 * The theme file, which controls the output of Concept Map module.
 */

 /**
  * Theme function for showing conceptmap entries in a table.
  *
  * @param array $entries
  *   An array of conceptmap relation database objects.
  */
function theme_conceptmap_relation_table($entries) {
  global $user;

  // Add the css for some base theming of the table.
  drupal_add_css(drupal_get_path('module', 'conceptmap') . '/css/conceptmap.css');

  // Some Variables to make this happen.
  $rows[] = array();
  // Setting up the table headers.
  $header = array();
  // These columns are always there.
  $header[] = array('data' => t('Source'));
  $header[] = array('data' => t('Description'));
  $header[] = array('data' => t('Destination'));

  // Need to add the operations column if permissions allow it.
  if (user_access('create conceptmap relation') || user_access('administer conceptmap')) {
    $header[] = array('data' => '');
  }

  // Loop through the entries and add them to the table.
  foreach ($entries as $entity) {
    if (user_access('create conceptmap relation')) {
      // Populate the row.
      $row = array(
        'data' => array(
          array(
            'data' => _conceptmap_get_tag_name($entity->source_tid),
            'class' => 'conceptmap_table_entry',
          ),
          array(
            'data' => $entity->description,
            'class' => 'conceptmap_table_entry italic',
          ),
          array(
            'data' => _conceptmap_get_tag_name($entity->destination_tid),
            'class' => 'conceptmap_table_entry',
          ),
        ),
        'class' => '',
        'no_striping' => TRUE,
      );

      if (user_access('create conceptmap relation') || user_access('administer conceptmap')) {
        // The owner can always edit relation entries.
        if ($user->uid == $entity->uid || user_access('administer conceptmap')) {
          // $destination = drupal_get_destination();
          // $destination = $destination['destination'];
          $edit_path = conceptmap_relation_uri($entity);
          $row['data'][] = array(
            'data' => l(t('edit'), $edit_path['path']),
            'class' => 'conceptmap_relation_edit',
          );
        }
        else {
          // If unrestricted positioning access is enabled, allow also editing.
          $conf = _conceptmap_load_settings();
          if (!$conf['drag']['resticted']) {
            $edit_path = conceptmap_relation_uri($entity);
            $row['data'][] = array(
              'data' => l(t('edit'), $edit_path['path']),
              'class' => 'conceptmap_relation_edit',
            );
          }
        }
      }
      $rows[] = $row;
    }
  }

  $form = array();

  $form['conceptmap_entries'] = array(
    '#type' => 'fieldset',
  );

  $table = array(
    'header' => $header,
    'rows' => $rows,
    'attributes' => array('class' => array('conceptmap_entries')),
  );

  $form['conceptmap_entries']['entry_table'] = array(
    '#markup' => theme('table', $table),
  );

  return drupal_render($form);
}

/**
 * Theme function for showing conceptmaps for term.
 *
 * @param array $entries
 *   An array of conceptmap relation database objects.
 */
function theme_conceptmap_display_map($entries) {
  global $user, $base_url;
  if (count($entries) <= 0) {
    $form['cmapview']['map'] = array(
      '#markup' => t('No entries found'),
    );
    return drupal_render($form);
  }

  // Add the css for some base theming of the table.
  drupal_add_css(drupal_get_path('module', 'conceptmap') . '/css/conceptmap.css');

  // Don't load javascript unless libraries module is present.
  if (module_exists('libraries')) {
    conceptmap_load_js();
  }
  $output = '';

  if (!isset($unique)) {
    $unique = uniqid();
  }
  conceptmap_init();
  $conf = _conceptmap_load_settings();
  foreach ($entries as $key => $entry) {
    $entity_main = entity_load_single($entry->entity_type, $entry->entity_id);
    // URL template %entity_type/%entity_bundle/%entity_id
    $reload_uri = $base_url . '/conceptmap/reload/' . $entry->entity_type . '/' . $entry->entity_bundle . '/' . $entry->entity_id;
    $eid = $entry->entity_id;
    $etype = $entry->entity_type;
    $ebundle = $entry->entity_bundle;
    $allow_moving = 0;
    // If user is the owner, let it move the tags on the concept map.
    if (isset($user->uid) && (($user->uid == $entity_main->uid) || user_access('administer site configuration'))) {
      $allow_moving = 1;
    }
    // If allowed in settings users can move the tags on the concept map.
    if (!$conf['drag']['resticted']) {
      $allow_moving = 1;
    }
  }

  $entries = _conceptmap_get_setting_value($eid, 'conceptmap_font_color');
  if ($entries) {
    foreach ($entries as $entity) {
      $conf['color']['font'] = $entity->value;
    }
  }
  $entries = _conceptmap_get_setting_value($eid, 'conceptmap_tag_background_color');
  if ($entries) {
    foreach ($entries as $entity) {
      $conf['color']['tag'] = $entity->value;
    }
  }
  $entries = _conceptmap_get_setting_value($eid, 'conceptmap_arrow_color');
  if ($entries) {
    foreach ($entries as $entity) {
      $conf['color']['arrow'] = $entity->value;
    }
  }
  // Now we create the processing sketch settings code.
  $conf['cmap_reload_uri'] = $reload_uri;
  $conf['cmap_wrapper'] = 'conceptmap-' . $unique;
  $conf['cmap_canvas_size'] = $conf['canvas']['size'];
  $author = $entity_main->title . ", " . format_username(user_load($entity_main->uid)) . ", " . format_date($entity_main->changed, 'custom', 'F Y');
  drupal_add_js(array('conceptmap' => $conf), array('type' => 'setting'));
  $pdecode = "\n";
  $pdecode .= 'color bgColor = color(' . $conf['color']['background'] . ", 255);\n";
  $pdecode .= 'color textColor = color(' . $conf['color']['font'] . ", 255);\n";
  $pdecode .= 'color tagColor = color(' . $conf['color']['tag'] . ", 255);\n";
  $pdecode .= 'color arrowColor = color(' . $conf['color']['arrow'] . ", " . $conf['color']['alpha'] . ");\n";
  $pdecode .= 'String author = "' . $author . "\";\n";
  $pdecode .= 'String uri = "' . $reload_uri . "\";\n";
  $pdecode .= 'String cid = "conceptmap-' . $unique . "\";\n";
  $pdecode .= 'int eid = ' . trim($eid) . ";\n";
  $pdecode .= 'String etype = "' . trim($etype) . '"' . ";\n";
  $pdecode .= 'String ebundle = "' . trim($ebundle) . '"' . ";\n";
  $pdecode .= 'int allow_moving = ' . $allow_moving . ";\n";
  $pdecode .= 'int arrowWidth = ' . $conf['arrow']['size'] . ";\n";
  $pdecode .= 'int border = ' . $conf['text']['margin'] . ";\n";
  $pdecode .= "\n";

  $pdecode .= 'int font_size = ' . $conf['font']['size'] . ";\n";
  $pdecode .= 'PFont txtfont = createFont("' . $conf['font']['type'] . '", font_size, 1)' . ";\n";
  $filepath = drupal_realpath(drupal_get_path('module', 'conceptmap') . '/js/conceptmap-display.js');
  $pdecode .= file_get_contents($filepath);

  if ($allow_moving) {
    // User is owner or access to move allowed, load the drag.js script.
    $filepath = drupal_realpath(drupal_get_path('module', 'conceptmap') . '/js/conceptmap-drag.js');
    $pdecode .= file_get_contents($filepath);
  }
  if (isset($user->uid) && ($user->uid == $entity_main->uid)) {
    // User is owner, load positioning.js script.
    drupal_add_js(drupal_get_path('module', 'conceptmap') . '/js/conceptmap-positioning.js', array('scope' => 'footer'));
  }
  else {
    if (!$conf['drag']['resticted']) {
      // Unrestricted positioning access.
      drupal_add_js(drupal_get_path('module', 'conceptmap') . '/js/conceptmap-positioning.js', array('scope' => 'footer'));
    }
  }

  // preg_replace( "/\r|\n/", "", $pdecode );
  $output .= '<span id="conceptmap-' . $unique . '-response" class="conceptmap-width"></span>';

  $output .= '<!--[if lt IE 9]>' . "\n";
  $output .= '<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>' . "\n";
  $output .= '<![endif]-->' . "\n";
  $output .= '<center>';
  $output .= '<script type="application/processing" data-processing-target="conceptmap-' . $unique . '" target="conceptmap-' . $unique . '" style="display: none">' . $pdecode . '</script>';
  $output .= '<canvas id="conceptmap-' . $unique . '" class="conceptmap-js-canvas">Upgrade your browser ...</canvas>';
  $output .= '</center>';

  $form['cmapview']['map'] = array(
    '#markup' => $output,
  );

  return drupal_render($form);
}
