/**
 * @file
 * Bind Concept Map if document ready.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - https://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($, Drupal, window, document, undefined) {

  function reloadConceptMap() {
    $.getJSON(Drupal.settings.conceptmap.cmap_reload_uri, function(data)  {
      if(!data.status || data.status == 0)  {
        if(data) {
          var pjs = Processing.getInstanceById(Drupal.settings.conceptmap.cmap_wrapper);
          // we know the JSON is an array of entries, called "entries"
          for (var key in data.entries) {
            var tag = data.entries[key];
            src = pjs.addTag(
              tag.source.tid,
              tag.source.name,
              tag.source.description,
              tag.source.position.x,
              tag.source.position.y,
              tag.source.position.z,
              tag.source.uri.path);
            dst = pjs.addTag(
              tag.destination.tid,
              tag.destination.name,
              tag.destination.description,
              tag.destination.position.x,
              tag.destination.position.y,
              tag.destination.position.z,
              tag.destination.uri.path);
            pjs.addRelation(
              src,
              dst,
              tag.description);
          }
        }
      }
    });
  }

  $(document).ready(function() {

    var bound = false;
    if(Drupal.settings.conceptmap) {
      var canvasWidth = $('#' + Drupal.settings.conceptmap.cmap_wrapper + "-response").width();
      var canvasHeight = Math.floor(canvasWidth / Drupal.settings.conceptmap.cmap_canvas_size);
      var ratio = canvasWidth / canvasHeight;
      // @todo: use ratio so we are responsive ... alert(ratio);
      // @todo: save a static image
      // var image = document.getElementsByTagName("canvas")[0].toDataURL();
    }
    function bindSketch() {
      if(Drupal.settings.conceptmap) {
        var pjs = Processing.getInstanceById(Drupal.settings.conceptmap.cmap_wrapper);
        if(pjs != null) {
          setTimeout(reloadConceptMap, 150);
          setInterval(reloadConceptMap, 15000);
          pjs.size(canvasWidth, canvasHeight);
          bound = true;
        }
        if(!bound) {
          setTimeout(bindSketch, 250);
        }
      }
    }
    if(!bound) {
      bindSketch();
    }
  });

})(jQuery, Drupal, this, this.document);
