/**
 * @file
 * Ajax callback to set Concept Map tag position.
 */
function setTagCoordinate(etype, ebundle, eid, tid , x, y) {
  var uPath = Drupal.settings.basePath + 'conceptmap/tagpos/' + etype + '/' + ebundle + '/' + eid + '/' + tid + '/' + x + '/' + y;
  jQuery.ajax({
    url: uPath,
    context: document.body,
    success: function(data){
      console.log("Updated Term ID: " + data);
    },
    statusCode: {
      404: function() {
      }
    },
    error: function() {
    },
  });
}
