/******************************************************************************************/
ArrayList tags, relations;

int sampleRate = 25;
float maxSpeed = 0.0;
// Mouse Vars.
int mouseDown = 0, dragging = - 1, moving = - 1, lastDragging = 0;
/******************************************************************************************/
void setup()
{
  // Size will be reset by jQuery on document.ready event.
  size(1213, 392, P2D);
  background(bgColor);
  frameRate(sampleRate);
  smooth();
  tags = new ArrayList();
  relations = new ArrayList();
  textFont(txtfont, font_size);
  textMode(SCREEN);
  cursor(CROSS);
  //noLoop();
  frameRate(8);
}

Relation addRelation(Tag stid, Tag dtid, String description)
{
  Relation rel = null;
  boolean exists = false;
  // check if we know about this relation
  for (int p = 0, end = relations.size(); p < end; p++) {
    rel = (Relation) relations.get(p);
    if ((stid.tid == rel.source.tid) && (dtid.tid == rel.destination.tid)) {
      exists = true;
      return rel;
    }
  }
  if (!exists) {
    rel = new Relation(stid, dtid, description);
    relations.add(rel);
  }
  return rel;
}

Tag addTag(int id, String name, String description, float x, float y, float z, String urlin)
{
  boolean exists = false;
  Tag tag = null;
  // Check if we know about this tag.
  for (int p = 0, end = tags.size(); p < end; p++) {
    tag = (Tag) tags.get(p);
    if (id == tag.tid) {
      // FIXME: may review .. should update coordinate.
      exists = true;
      return tag;
    }
  }
  if(!exists) {
    if ((x <= 0) && (y <= 0)) {
      // Maybe a new item, while users can not place items at this position.
      x = random(3 * width / 10, 8 * width / 10);
      y = random(3 * height / 10, 8 * height / 10);
    } else {
      x = random(10 * x / 10, 10 * x / 10);
      y = random(10 * y / 10, 10 * y / 10);
    }
    //alert(x + "," + y + "|" + width + "x" + height);
    tag = new Tag(x, y, z, name, description, id, urlin);
    tags.add(tag);
  }
  return tag;
}

void draw()
{
  Tag tag;
  Relation rel;
  noStroke();
  fill(bgColor);
  rect(0, 0, width, height);
  // Add author informations.
  textSize(font_size / 1.5);
  stroke(tagColor);
  fill(tagColor);
  text(author, 11, 11 + textDescent());

  for (int p = 0, end = relations.size(); p < end; p++) {
    rel = (Relation) relations.get(p);
    rel.draw();
  }

  for (int q = 0, qend = tags.size(); q < qend; q++) {
    tag = (Tag) tags.get(q);
    tag.draw();
  }

}

/******************************************************************************************/
class Relation
{
  String description = "";
  Tag source;
  Tag destination;
  PVector position;
  float boxWidth, boxHeight;
  int lastArrow = 0;

  // Set up initial position.
  Relation (Tag sin, Tag din, String txt)
  {
    source = sin;
    destination = din;
    description = txt;
    position = new PVector(0, 0);
    draw();
  }

  void draw()
  {
    PVector inVertex, outVertex;
    float dx, dy;

    stroke(arrowColor);
    strokeWeight(font_size / 20);
    fill(arrowColor);

    inVertex = new PVector(source.position.x, source.position.y);
    outVertex = new PVector(destination.position.x, destination.position.y);
    dx = inVertex.x - outVertex.x;
    dy = inVertex.y - outVertex.y;

    stroke(arrowColor);
    fill(arrowColor);
    noStroke();

    if ((abs(dy) <= source.boxHeight)) {
      // Bezier curve: from source to destination.
      stroke(arrowColor);
      noFill();
      if (dx <= 0) {
        bezier(
          inVertex.x, inVertex.y,
          inVertex.x + source.boxWidth * 2, inVertex.y + source.boxHeight * 2,
          outVertex.x - destination.boxWidth * 2, outVertex.y - destination.boxHeight * 2,
          outVertex.x, outVertex.y
        );
      }
      else {
       bezier(
          inVertex.x, inVertex.y,
          inVertex.x - source.boxWidth * 2, inVertex.y - source.boxHeight * 2,
          outVertex.x + destination.boxWidth * 2, outVertex.y + destination.boxHeight * 2,
          outVertex.x, outVertex.y
        );
      }
    }
    else {
      // Half triangle: from relation text to destination.
      triangle(inVertex.x - source.boxWidth / 33 - arrowWidth, inVertex.y , inVertex.x + source.boxWidth / 33 + arrowWidth, inVertex.y , destination.position.x + dx / 10, destination.position.y + dy / 10);
      fill(bgColor);
      noStroke();
      // Half triangle: from relation text to destination.
      triangle(inVertex.x - source.boxWidth / 100 - arrowWidth, inVertex.y , inVertex.x + source.boxWidth / 100 + arrowWidth, inVertex.y , destination.position.x + dx / 5, destination.position.y + dy / 5);
    }

    // Draw relation textbox.
    drawRelation(source.position.x, source.position.y, destination.position.x, destination.position.y);
  }

  void drawRelation(float sx, float sy, float dix, float diy)
  {
    //stroke(arrowColor);
    noStroke();
    float t = font_size;
    PVector inVertex = new PVector(sx, sy);
    PVector outVertex = new PVector(dix, diy);
    float dx = inVertex.x - outVertex.x;
    float dy = inVertex.y - outVertex.y;
    position.x = sx - dx / 2;
    position.y = sy - dy / 2;

    textSize(font_size / 1.5);
    float tagHeight = textAscent() + textDescent();
    float tagWidth = textWidth(description);
    boxWidth = tagWidth + 2 * border + tagWidth / 3;
    boxHeight = tagHeight + 2 * border + tagHeight / 3;
    strokeWeight(font_size / 20);
    drawBox(position.x, position.y - (tagHeight / 2), boxWidth, boxHeight + tagHeight);
    // Relation text within text box.
    fill(tagColor);
    text(description, position.x - tagWidth / 2, position.y - textDescent());
  }

  private void drawBox(float x, float y, float w, float h) {
    int hi = 3 * h / 4;
    fill(bgColor);
    strokeWeight((int)font_size / 10);
    stroke(bgColor);
    rect(x - w / 2, y - hi / 2, w, hi, arrowWidth);
  }
}

/******************************************************************************************/
class Tag
{
  float tempxpos, tempypos;
  PVector velocity;
  PVector position;
  float tagWidth, tagHeight;
  float boxWidth, boxHeight;
  String name = "";
  String description = "";
  String url = "";
  int tid = 0;
  float weight = -90;
  float spring = 0.007234;
  private boolean move = false;
  // Set drag switch to false.
  boolean dragging = false;
  boolean hover = false;

  // Set up initial position.
  Tag (float xin, float yin, float zin, String namein, String descin, int tidin, String urlin)
  {
    tid = tidin;
    position = new PVector(xin, yin, zin);
    name = namein;
    description = descin;
    url = urlin;
    velocity = new PVector(random(-maxSpeed,maxSpeed), random(-maxSpeed,maxSpeed));

    // Need to draw the font before we can know and set the width.
    tagWidth = textWidth(name) + 2 * border;
    tagHeight = - 1 * textAscent() + textDescent();
    position.add(velocity);
  }

  void update()
  {
    position.add(velocity);
    // Slow down, take it easy.
    checkBoundaryCollision();
  }

  void checkBoundaryCollision()
  {
    // Check horizontal edges.
    if (position.x > (width - boxWidth / 2)) {
      velocity.x *= - 1;
      position.x = (width - boxWidth / 2);
    } else if (position.x < (boxWidth / 2)) {
      velocity.x *= - 1;
      position.x = boxWidth / 2;
    } else {
      velocity.x = velocity.x - velocity.x / (random(50,60) * 1);
    }

    // Check vertical edges.
    if (position.y > (height - (boxHeight / 2))) {
      velocity.y *= - 1;
      position.y = height - (boxHeight / 2);
    } else if (position.y < (boxHeight)) {
      velocity.y *= - 1;
      position.y = (boxHeight);
    } else {
      velocity.y = velocity.y - velocity.y / (random(50,60) * 1);
    }
  }

  void draw()
  {
    update();
    // Draw tag at current position.
    textSize(font_size);
    tagWidth = textWidth(name);
    tagHeight = textAscent() + textDescent();
    boxWidth = tagWidth + 2 * border + tagWidth / 3;
    boxHeight = tagHeight + 2 * border + tagHeight / 3;
    drawBox(position.x, position.y - (tagHeight / 2), boxWidth, boxHeight + tagHeight);
    fill(textColor);
    stroke(textColor);
    strokeWeight(font_size);
    text(name, position.x - tagWidth / 2, position.y - textDescent() / 2);
  }

  private void drawBox(float x, float y, float w, float h) {
    int hi = 3 * h / 4;
    fill(tagColor);
    strokeWeight(font_size / 10);
    stroke(bgColor);
    rect(x - w / 2, y - hi / 2, w, hi, arrowWidth);
  }
}
/******************************************************************************************/
