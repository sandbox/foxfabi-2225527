int hoverDetect()
{
  Tag tag;
  int lastHover = 0;
  for(int q = 0, qend = tags.size(); q < qend; q++) {
    tag = (Tag) tags.get(q);
    if (mouseX <= tag.position.x + tag.boxWidth / 2 && mouseX >= tag.position.x - tag.boxWidth / 2 &&
        mouseY <= tag.position.y + tag.boxWidth / 2 && mouseY >= tag.position.y - tag.boxHeight / 2) {
      tag.hover = true;
      dragging = tag.tid;
      moving = tag.tid;
    } else {
      tag.hover = false;
    }
  }
  return dragging;
}

void mouseMoved()
{
  dragging = hoverDetect();
}

void mousePressed()
{
  if (dragging == -1) {
    thisN = hoverDetect();
    dragging = thisN;
  }
}

void mouseReleased()
{
  if (!allow_moving) {
    return;
  }
  lastDragging = dragging;
  dragging = -1;
  if (lastDragging > -1) {
    for(int q = 0, qend = tags.size(); q < qend; q++) {
      Tag tag = (Tag) tags.get(q);
      if (tag.tid == moving && tag.hover) {
        if(typeof setTagCoordinate == 'function') {
          setTagCoordinate(etype, ebundle, eid, tag.tid, tag.position.x, tag.position.y);
        }
      }
    }
  }
}

void mouseDragged()
{
  Tag tag;
  if (!allow_moving) {
    return;
  }
  if (dragging > -1) {
    for (int q = 0, qend = tags.size(); q < qend; q++) {
      tag = (Tag) tags.get(q);
      if (tag.tid == dragging && tag.hover) {
        tag.position.x = mouseX;
        tag.position.y = mouseY;
        tag.hover = true;
      }
    }
  }
}
