<?php
/**
 * @file
 * Provide views data for conceptmap.module.
 */

/**
 * Implements hook_views_data().
 */
function conceptmap_views_data() {

  // Define the base group of this table.
  $data['conceptmap']['table']['group'] = t('Concept Map table');

  // Define this as a base table describing itself by for views.
  $data['conceptmap']['table']['base'] = array(
    'field' => 'relation_id',
    'title' => t('Concept Map table'),
    'help' => t('Table containing conceptmap relations.'),
    'weight' => -10,
  );

  // This table references the {taxonomy_term} table.
  $data['conceptmap']['table']['join'] = array(
    // Index this array by the table name to which this table refers.
    // 'left_field' is the primary key in the referenced table.
    // 'field' is the foreign key in this table.
    'source' => array(
      'left_field' => 'tid',
      'field' => 'source_tid',
    ),
    'destination' => array(
      'left_field' => 'tid',
      'field' => 'destination_tid',
    ),
  );

  // Description field.
  $data['conceptmap']['description'] = array(
    'title' => t('Relation description'),
    'help' => t('Description of relations.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Language field.
  $data['conceptmap']['language'] = array(
    'title' => t('Language'),
    'help' => t('Language of relations.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Created field.
  $data['conceptmap']['created'] = array(
    'title' => t('Post date'),
    'help' => t('The date the content was posted.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  // Changed field.
  $data['conceptmap']['changed'] = array(
    'title' => t('Updated date'),
    'help' => t('The date the term was last updated.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  // Uid field.
  $data['conceptmap']['uid'] = array(
    'title' => t('Author uid'),
    'help' => t('The user authoring the relation. If you need more fields than the uid add the content: author relationship.'),
    'relationship' => array(
      'title' => t('Author'),
      'help' => t('Relate relations to the user who created it.'),
      'handler' => 'views_handler_relationship',
      'base' => 'users',
      'field' => 'uid',
      'label' => t('author'),
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_user_name',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'field' => array(
      'handler' => 'views_handler_field_user',
    ),
  );

  return $data;
}
