<?php
/**
 * @file
 * Page callback file for the concept module.
 */

/**
 * Menu callback that displays a page containing recent concept map of a user.
 *
 * @param Object $account
 *   User object to show entries for.
 */
function conceptmap_page_user($account) {
  global $user;
  if (!isset($account)) {
    $account = $user;
  }

  drupal_set_title($title = t("@name's concept maps", array('@name' => format_username($account))), PASS_THROUGH);

  $build = array();

  $query = db_select('node', 'n')->extend('PagerDefault');
  $nids = $query
    ->fields('n', array('nid', 'sticky', 'created'))
    ->condition('type', 'conceptmap')
    ->condition('uid', $account->uid)
    ->condition('status', 1)
    ->orderBy('sticky', 'DESC')
    ->orderBy('created', 'DESC')
    ->limit(variable_get('default_nodes_main', 10))
    ->addTag('node_access')
    ->execute()
    ->fetchCol();

  if (!empty($nids)) {
    $nodes = node_load_multiple($nids);
    // Theme it as a table.
    foreach ($nodes as $key => $entity) {
      $spath = entity_uri('node', $entity);
      $rows[] = array(
        'data' => array(
          l($entity->title, $spath['path']),
          format_date($entity->created, 'short'),
        ),
        'class' => array('draggable'),
      );
    }
    // The table headers.
    $header = array(
      t('Concept Map'),
      t('Created'),
    );

    $table = array(
      'header' => $header,
      'rows' => $rows,
      'attributes' => array('id' => array('conceptmap-table')),
    );

    $build = theme('table', $table);

  }
  else {
    if ($account->uid == $user->uid) {
      drupal_set_message(t('You have not created any concept maps.'));
    }
    else {
      drupal_set_message(t('!author has not created any concept maps.', array('!author' => theme('username', array('account' => $account)))));
    }
  }

  return $build;
}
