<?php
/**
 * @file
 * Install, update, and uninstall functions for conceptmap.
 */

/**
 * Implements hook_install().
 */
function conceptmap_install() {
  drupal_static_reset();
  taxonomy_vocabulary_static_reset();
  cache_clear_all();
  // During installation, the t() function is unavailable.
  $t = get_t();

  // Create a default vocabulary named "Tags", if not available.
  module_load_include('module', 'taxonomy', 'taxonomy');
  $vocabulary = taxonomy_vocabulary_machine_name_load('tags');

  if (empty($vocabulary->vid) && empty($vocabulary->name)) {
    $description = $t('Use tags to group articles on similar topics into categories.');
    $help = $t('Enter a comma separated list of words to describe your content.');
    $vocabulary = (object) array(
      'name' => $t('Tags'),
      'description' => $description,
      'machine_name' => 'tags',
      'module' => 'conceptmap',
      'help' => $help,

    );
    taxonomy_vocabulary_save($vocabulary);
    if (!empty($vocabulary->name)) {
      drupal_set_message($t("Added vocabulary '@tags' ...", array('@tags' => $vocabulary->name)));
    }
  }

  // Check if the field_tags is already created.
  if (!field_info_field('field_' . $vocabulary->machine_name)) {
    $field = array(
      'field_name' => 'field_' . $vocabulary->machine_name,
      'type' => 'taxonomy_term_reference',
      // Set cardinality to unlimited for tagging.
      'cardinality' => FIELD_CARDINALITY_UNLIMITED,
      'settings' => array(
        'allowed_values' => array(
          array(
            'vocabulary' => $vocabulary->machine_name,
            'parent' => 0,
          ),
        ),
      ),
    );
    field_create_field($field);
    drupal_set_message($t("Field 'field_tags' created ..."));
  }

  // Update node types cache.
  node_types_rebuild();
  $types = node_type_get_types();
  node_add_body_field($types['conceptmap']);

  // Add field_tags instance to conceptmap node, if prior_instance not exists.
  $help = $t('Enter a list of words to describe your content.');
  $instance = array(
    'field_name' => 'field_' . $vocabulary->machine_name,
    'entity_type' => 'node',
    'label' => 'Tags',
    'required' => TRUE,
    'bundle' => 'conceptmap',
    'description' => $help,
    'widget' => array(
      'type' => 'taxonomy_autocomplete',
      'weight' => -4,
    ),
    'display' => array(
      'default' => array(
        'type' => 'taxonomy_term_reference_link',
        'weight' => 10,
      ),
      'teaser' => array(
        'type' => 'taxonomy_term_reference_link',
        'weight' => 10,
      ),
    ),
  );
  $prior_instance = field_read_instance($instance['entity_type'], $instance['field_name'], $instance['bundle']);
  if (empty($prior_instance)) {
    field_create_instance($instance);
    drupal_set_message($t("Concept Map 'field_tags' created..."));
  }

  // Get the administrator role.
  $admin_rid = variable_get('user_admin_role', FALSE);
  if ($admin_rid) {
    user_role_grant_permissions($admin_rid, array_keys(module_invoke('conceptmap', 'permission')));
  }
  else {
    $admin_role = user_role_load_by_name('administrator');
    if (!empty($admin_role)) {
      if ($admin_role->rid) {
        user_role_grant_permissions($admin_role->rid, array_keys(module_invoke('conceptmap', 'permission')));
      }
    }
  }

  // Rebuilds the node access database.
  $batch_mode = FALSE;
  node_access_rebuild($batch_mode);

  // Add pathauto default pattern.
  if (module_exists('pathauto')) {
    variable_set('pathauto_node_conceptmap_pattern', 'conceptmap/[node:title]');
  }

  // Clear cache.
  cache_clear_all();

  drupal_set_message($t("Concept Map has been installed successfully."));
  drupal_set_message($t("Configure concept mapping settings.") . ' ' . l($t('Concept Map settings'), 'admin/config/user-interface/conceptmap'));
}

/**
 * Implements hook_uninstall().
 *
 * At uninstall time we'll notify field.module that the entity was deleted
 * so that attached fields can be cleaned up.
 */
function conceptmap_uninstall() {
  // Remove unused conceptmap field_tags instance.
  if ($instance = field_info_instance('node', 'field_tags', 'conceptmap')) {
    field_delete_instance($instance);
  }

  // Delete used variables.
  variable_del('conceptmap_drag_restict');
  variable_del('conceptmap_show_description');
  variable_del('conceptmap_default_collapsed');
  variable_del('conceptmap_font');
  variable_del('conceptmap_font_size');
  variable_del('conceptmap_canvas_size');
  variable_del('conceptmap_arrow_size');
  variable_del('conceptmap_text_margin');
  variable_del('conceptmap_background_color');
  variable_del('conceptmap_font_color');
  variable_del('conceptmap_tag_background_color');
  variable_del('conceptmap_arrow_color');
  variable_del('conceptmap_color_alpha');

  drupal_set_message(t("Concept Map has been removed successfully."));
}

/**
 * Implements hook_schema().
 */
function conceptmap_schema() {
  $schema = array();

  $schema['conceptmap_setting'] = array(
    'description' => 'The base table for concept map settings.',
    'fields' => array(
      'etid' => array(
        'type' => 'serial',
        'not null' => TRUE,
        'unsigned' => TRUE,
      ),
      'entity_type' => array(
        'type' => 'varchar',
        'length' => 100,
      ),
      'entity_bundle' => array(
        'type' => 'varchar',
        'length' => 100,
      ),
      'entity_id' => array(
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'default' => 0,
      ),
      'uid' => array(
        'description' => 'ID of author or owner.',
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'default' => 0,
      ),
      'name' => array(
        'type' => 'varchar',
        'length' => 255,
      ),
      'value' => array(
        'type' => 'varchar',
        'length' => 255,
      ),
    ),
    'primary key' => array('etid'),
    'unique keys' => array(
      'unique_id' => array(
        'entity_type',
        'entity_bundle',
        'entity_id',
        'name',
        'value'),
    ),
    'indexes' => array(
      'name_value' => array('name', 'value'),
    ),
    'foreign keys' => array(
      'uid' => array(
        'table' => 'users',
        'columns' => array('uid' => 'uid'),
      ),
    ),
  );

  $schema['conceptmap_relation'] = array(
    'description' => 'The base table for concept map relations.',
    'fields' => array(
      'relation_id' => array(
        'type' => 'serial',
        'not null' => TRUE,
        'unsigned' => TRUE,
      ),
      'entity_type' => array(
        'type' => 'varchar',
        'length' => 100,
      ),
      'entity_bundle' => array(
        'type' => 'varchar',
        'length' => 100,
      ),
      'entity_id' => array(
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'default' => 0,
      ),
      'uid' => array(
        'description' => 'ID of relation author or owner.',
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'default' => 0,
      ),
      'source_tid' => array(
        'description' => 'ID of relation source term.',
        'type' => 'int',
        'not null' => TRUE,
      ),
      'description' => array(
        'type' => 'text',
      ),
      'destination_tid' => array(
        'description' => 'ID of relation destination term.',
        'type' => 'int',
        'not null' => TRUE,
      ),
      'language' => array(
        'type' => 'varchar',
        'length' => 255,
      ),
      'created' => array(
        'description' => 'The Unix timestamp when the item was created.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'changed' => array(
        'description' => 'The Unix timestamp when the item was most recently saved.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'primary key' => array('relation_id'),
    'unique keys' => array(
      'unique_id' => array(
        'entity_type',
        'entity_bundle',
        'entity_id',
        'source_tid',
        'destination_tid'),
    ),
    'indexes' => array(
      'src_dst' => array('source_tid', 'destination_tid'),
    ),
    'foreign keys' => array(
      'source_tid' => array(
        'table' => 'taxonomy_term_data',
        'columns' => array('source_tid' => 'tid'),
      ),
      'destination_tid' => array(
        'table' => 'taxonomy_term_data',
        'columns' => array('destination_tid' => 'tid'),
      ),
      'uid' => array(
        'table' => 'users',
        'columns' => array('uid' => 'uid'),
      ),
    ),
  );

  $schema['conceptmap_tag_position'] = array(
    'description' => 'The base table for concept map tag positions.',
    'fields' => array(
      'position_id' => array(
        'type' => 'serial',
        'not null' => TRUE,
        'unsigned' => TRUE,
      ),
      'entity_type' => array(
        'type' => 'varchar',
        'length' => 100,
      ),
      'entity_bundle' => array(
        'type' => 'varchar',
        'length' => 100,
      ),
      'entity_id' => array(
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'default' => 0,
      ),
      'uid' => array(
        'type' => 'int',
        'description' => 'ID of owner.',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'default' => 0,
      ),
      'tid' => array(
        'description' => 'ID of term.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'x' => array(
        'description' => 'X position of term.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'y' => array(
        'description' => 'Y position of term.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'z' => array(
        'description' => 'Z position of term.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'created' => array(
        'description' => 'The Unix timestamp when the item was created.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'changed' => array(
        'description' => 'The Unix timestamp when the item was recently saved.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'primary key' => array('position_id'),
    'unique keys' => array(
      'unique_id' => array(
        'entity_type',
        'entity_bundle',
        'entity_id',
        'uid',
        'tid'),
    ),
    'indexes' => array(
      'uid_tid' => array('uid', 'tid'),
    ),
    'foreign keys' => array(
      'tid' => array(
        'table' => 'taxonomy_term_data',
        'columns' => array('tid' => 'tid'),
      ),
      'uid' => array(
        'table' => 'users',
        'columns' => array('uid' => 'uid'),
      ),
    ),
  );

  return $schema;
}
