<?php
/**
 * @file
 * Entity related helper functions.
 */

/**
 * Implements a uri callback.
 */
function conceptmap_relation_uri($conceptmap_relation) {
  return array(
    'path' => 'conceptmap/edit/' . $conceptmap_relation->relation_id,
  );
}

/**
 * Implements a uri callback.
 */
function conceptmap_relation_delete_uri($conceptmap_relation) {
  return array(
    'path' => 'conceptmap/delete/' . $conceptmap_relation->relation_id,
  );
}

/**
 * We save the entity by calling the controller.
 */
function conceptmap_relation_save(&$entity) {
  $controller = entity_get_controller('conceptmap_relation');
  $entity = $controller->save($entity);
  // Check if term position already exists.
  $entries = $controller->getPosition($entity->uid, $entity->source_tid, $entity->entity_type, $entity->entity_bundle, $entity->entity_id, FALSE);
  if (count($entries) <= 0) {
    // Add dummy positions for source term.
    $pos['src'] = $controller->addPosition($entity->uid, $entity->source_tid, $entity->entity_type, $entity->entity_bundle, $entity->entity_id);
  }
  $entries = $controller->getPosition($entity->uid, $entity->destination_tid, $entity->entity_type, $entity->entity_bundle, $entity->entity_id, FALSE);
  if (count($entries) <= 0) {
    // Add dummy positions for destination term.
    $pos['dst'] = $controller->addPosition($entity->uid, $entity->destination_tid, $entity->entity_type, $entity->entity_bundle, $entity->entity_id);
  }
  return $entity;
}

/**
 * Use the controller to delete the entity.
 */
function conceptmap_relation_delete($entity) {
  entity_get_controller('conceptmap_relation')->delete($entity);
}

/**
 * Use the controller to delete multiple entries.
 */
function conceptmap_relation_delete_multiple($entities) {
  entity_get_controller('conceptmap_relation')->deleteMultiple($entities);
}

/**
 * We save the entity by calling the controller.
 */
function conceptmap_tag_position_save(&$entity) {
  return entity_get_controller('conceptmap_tag_position')->save($entity);
}
